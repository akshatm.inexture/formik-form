import React from 'react'
import FormikForm from './Formik-Form'
import HookForm from './Hook-Form'

function App() {
  return (
    <div>
     {/* <FormikForm/>*/}
     <HookForm/>
      </div>
  )
}

export default App
