import React from "react";
import { ErrorMessage } from "formik";

const ErrMsg = ({ name }) => {
  return (
    <div style={{ color: "red" ,fontWeight:"bold"}}>
      <br />
      <ErrorMessage name={name} />
    </div>
  );
};

export default ErrMsg;
