import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import ErrMsg from "./Error";

const validationSchema = yup.object({
  //Name Validation
  name: yup
  .string()
  .required("Name is Must Added!"),

  //Phone Number Validation
  phone: yup
    .number()
    .min(1000000000, "Add a Valid Phone Number!")
    .max(9999999999, "Add a Valid Phone Number!")
    .required("Phone-Number is Required!"),

  //Password Validation
  password: yup
    .string()
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase Letter, One Lowercase Letter, One Number Letter and one special case Character"
    )
    .required("Password is Required!"),

    //Gender Validation
  gender: yup
  .string()
  .required("Select Any One Gender....."),

  //Date Validation
  date: yup
  .date()
  .required("Date of Birth is required......"),

  //Income Validation
  income: yup
  .string()
  .required("Select Your Income Range....."),

  //About Validation
  about: yup
    .string()
    .min(5, "Too Small Description.....")
    .max(500, "Too Long Description.....")
    .required("Describe Yourself in Between 5 to 495 Characters..... "),

});
const FormikForm= () => {
  return (
    <div>
      <h1><center>React Formik form with Yup Library Validation...</center></h1>
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          name: "",
          phone: "",
          password: "",
          gender: "",
          date: "",
          income: "",
          about: "",
        }}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {({ values }) => (
          <Form>

            <label>Name:</label>
            <Field name="name" type="text" />
            <ErrMsg name="name" />
            <br /> 

            <label>Phone:</label>
            <Field name="phone" type="number" />
            <ErrMsg name="phone" />
            <br /> 

            <label>Password:</label>
            <Field name="password" type="password" />
            <ErrMsg name="password" />
            <br /> 

            <label>Gender:</label>
            <label>Male:</label>
            <Field name="gender" value="male" type="radio" />
            <label>Female:</label>
            <Field name="gender" value="female" type="radio" />
            <ErrMsg name="gender" />
            <br /> 

            <label>Date:</label>
            <Field name="date" type="date" />
            <ErrMsg name="date" />
            <br /> 
            
            <label>Income:</label>
            <Field name="income" as="select">
              <option value="0">RS. 0</option>
              <option value="5000">RS. 5000</option>
              <option value="10000">RS. 10000</option>
              <option value="125000">Rs. 125000</option>
            </Field>
            <ErrMsg name="income" />
            <br /> <br />

            <label>Describe Yourself:</label>
            <Field name="about" as="textarea" />
            <ErrMsg name="about" />
            <br /> <br />

           
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default FormikForm;