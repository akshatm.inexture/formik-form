import React from "react"
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import './App.css'

const schema = yup.object().shape({
  //First Name Validation
  firstName: yup
  .string()
  .required("Name is Must Added!"),

   //Last Name Validation
   lastName: yup
   .string()
   .required("Name is Must Added!"),


   //Email Validation
   email: yup
   .string()
   .email('Must be a valid email please')
   .max(255)
   .required('Email is required'),


   //Age Validation
   age: yup
   .number()
   .positive('Enter a Positive Number Only...')
   .integer()
   .required('Select Your Age.....'),

  //Password Validation
  password: yup
    .string()
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase Letter, One Lowercase Letter, One Number Letter and one special case Character"
    )
    .required("Password is Required!"),


    confirmpassword: yup
    .string()
    .oneOf([yup.ref("password"), null])
   

});


function HookForm() {

    const { register, handleSubmit,formState:{ errors} } = useForm({
        resolver: yupResolver(schema)
      });
    
      const submitForm = (data) => {
        console.log(data);
      };
  return (
    <div className="HookFormStyle">
      <h1><center>React Hook form with Yup Library Validation...</center></h1>
        <form onSubmit={handleSubmit(submitForm)}>
      <input type='text' name='firstName' placeholder='Enter Your First Name' {...register("firstName")}></input>
      <p> {errors.firstName?.message} </p>

      <input type='text' name='lastName' placeholder='Enter Your Surname' {...register("lastName")}></input>
      <p> {errors.lastName?.message} </p>

      <input type='text' name='email' placeholder='Enter Your Email Id' {...register("email")}></input>
      <p> {errors.email?.message} </p>

      <input type='text' name='age' placeholder='Enter Your Age' {...register("age")}></input>
      <p> {errors.age?.message} </p>

      <input type='text' name='password' placeholder='Enter password' {...register("password")}></input>
      <p> {errors.password?.message} </p>

      <input type='text' name='confirmpassword' placeholder='confirm password' {...register("confirmpassword")}></input>
      <p> {errors.confirmpassword && 'Password Not Matches...'} </p>
      <input type='submit'></input>
      </form>
    </div>
  )
}

export default HookForm